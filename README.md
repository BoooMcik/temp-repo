## Magnet

 **1. Postgress Tables**  
	  - MagnetCommonWords - table for common words  
	  - MagnetHistory - table for magnet history search results. Difference with CerebroHistory table in columns asin and keyword. In magnet history we store history by keyword  
**2. Methods**  
 - **customer\modules\magnet\models\ReverseSearch::getFromApi** - method got getting top 10 ASIN's from MWS API by jeyword. Used if we have no results in Clickhouse by search keyword. Used in: **customer\modules\magnet\models\ReverseSearch** on lines 304 and 308.  
 - **customer\modules\magnet\models\ReverseSearch::appendImagesAndTitle**  - method for append images and title for rop3 asins used in Block on search results page. Used in **customer\modules\magnet\models\ReverseSearch** on line 405  
 - **customer\modules\magnet\models\ReverseSearch::calculateCommonWords** - method for calculating data for block "Related Words" on search results page. Used in **customer\modules\magnet\models\controllers\MainController** on line 614.  
  
 **3. Difference with Cerebro**  
 Difference is in getting results logic. It's implemented in **customer\modules\magnet\models\ReverseSearch** and starts on line  282. We get phrase id on line 282 and get ASINS with this phrase id from clickhouse from line 283 to 299. That query contain criteria position and weeks. This data comes from fields "from" "to" "weeks" on index page and sends to ReverseSearch model with paramethers :  
 **positionFromPGQuery**  - field "from"  
 **positionToPGQuery**  - field "to"  
 **weeksForPG**  - field "weeks".  
    
 If we have no phrase in our PG table or have not results in clickhouse by that keyword we get 10 ASINS from MWS API on lines from 303 to 308. From line 314 to line 327 we get top 3 asins for block with same name for search results page and generate asins string for clickhouse query for getting keywords by ASIN's. On lines 335 - 341 we have query for getting keywords from clickhouse. That query has same criterias with first query for weeks paramethers and position by with different fields and paramether names. In form "from1", "to1", "weeks1" and in model this have names  
  **positionFromClickhouseQuery**  - field "from1"  
  **positionToClickhouseQuery**  - field "to1"  
  **weeksForClickhouse**  - field "weeks1".  
  Default values for all position "from" fields is **1**. Default  values for all "to" fields is "6". Default values for all weeks fields is "10".